---
layout: default
custom_css: /css/home.css
---

<table id='portrait' class='bordered container'>
    <tbody>
        <tr>
            <td>
                <img class="smooth" alt="     " src="/assets/images/helloworld.jpg"/>
            </td>
        </tr>
        <tr>
            <th><i>fig. a: Silas Bartha, probably.</i></th>
        </tr>
    </tbody>
</table>

**Silas Bartha: Computer Creature**

Welcome to my website. I'm a software engineering student located in Waterloo, Ontario, and a hobbyist developer.

I have many interests, which I hope to document through this site. You may notice that many of the navigation options are <span>o̵̳̥̲̿̊͊́̑͝u̶̢̻̮̘̼͆ț̷̨̥̓͛̿ ̸̛̬̻̪̮̱͐̽̿̕o̴͓͓̻̩̩͔̽̈́͑͐̉͗f̸̡̬͎͈͇̎̿̎͝ͅ ̷̛̹͐̌͂o̴͇̳͎̝̫̥͐r̵̟͓͚̤͑̂̐̚ḓ̵̛͔̞͔̩̈e̷̢̻̓̽̈́r̸̨̙̭͋͂</span>{: style="color:red;"} at the moment, but I can assure you that I'll be rectifying these portions of the site as frequently as my incredibly busy student schedule allows.

Until then, feel free to check back frequently and witness Silas Bartha. I can be contacted at [silas@exvacuum.dev](mailto:silas@exvacuum.dev) for any inquiries or other communications.

